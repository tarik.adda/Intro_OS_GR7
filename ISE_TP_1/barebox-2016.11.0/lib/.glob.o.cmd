cmd_lib/glob.o := arm-none-eabi-gcc -Wp,-MD,lib/.glob.o.d  -nostdinc -isystem /usr/lib/gcc/arm-none-eabi/4.9.3/include -D__KERNEL__ -D__BAREBOX__ -Iinclude -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/dts/include  -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include -include /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/include/linux/kconfig.h -fno-builtin -ffreestanding -D__ARM__ -fno-strict-aliasing -marm -mlittle-endian -mabi=aapcs-linux -mno-thumb-interwork -D__LINUX_ARM_ARCH__=7 -march=armv7-a  -msoft-float  -Iarch/arm/mach-vexpress/include -DTEXT_BASE=0x63f00000 -fdata-sections -ffunction-sections -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -Werror-implicit-function-declaration -fno-strict-aliasing -fno-common -Os -pipe -mno-unaligned-access  -fno-stack-protector -Wno-unused-but-set-variable -Wno-trampolines  -fno-delete-null-pointer-checks -Wdeclaration-after-statement -Wno-pointer-sign    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(glob)"  -D"KBUILD_MODNAME=KBUILD_STR(glob)" -c -o lib/glob.o lib/glob.c

source_lib/glob.o := lib/glob.c

deps_lib/glob.o := \
    $(wildcard include/config/glob.h) \
    $(wildcard include/config/glob/sort.h) \
    $(wildcard include/config/fake/glob.h) \
  include/common.h \
    $(wildcard include/config/banner.h) \
    $(wildcard include/config/mips.h) \
  include/stdio.h \
    $(wildcard include/config/console/none.h) \
    $(wildcard include/config/pbl/console.h) \
  /usr/lib/gcc/arm-none-eabi/4.9.3/include/stdarg.h \
  include/console.h \
    $(wildcard include/config/cbsize.h) \
    $(wildcard include/config/prompt.h) \
  include/param.h \
    $(wildcard include/config/parameter.h) \
  include/linux/err.h \
  include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/kasan.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
    $(wildcard include/config/kprobes.h) \
  include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
    $(wildcard include/config/gcov/kernel.h) \
    $(wildcard include/config/arch/use/builtin/bswap.h) \
  include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
  include/linux/posix_types.h \
  include/linux/stddef.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/posix_types.h \
  include/asm-generic/posix_types.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/bitsperlong.h \
  include/asm-generic/bitsperlong.h \
    $(wildcard include/config/64bit.h) \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/types.h \
  include/asm-generic/errno.h \
  include/linux/list.h \
    $(wildcard include/config/debug/list.h) \
  include/driver.h \
    $(wildcard include/config/driver/net/dm9k.h) \
    $(wildcard include/config/usb/ehci.h) \
    $(wildcard include/config/driver/net/ks8851/mll.h) \
    $(wildcard include/config/ofdevice.h) \
  include/linux/ioport.h \
  include/of.h \
    $(wildcard include/config/oftree.h) \
  include/fdt.h \
  include/errno.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/byteorder.h \
  include/linux/byteorder/little_endian.h \
  include/linux/swab.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/swab.h \
  include/linux/byteorder/generic.h \
  include/module.h \
    $(wildcard include/config/modules.h) \
  include/elf.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/elf.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/ptrace.h \
    $(wildcard include/config/arm/thumb.h) \
  include/config.h \
  include/clock.h \
  include/types.h \
  include/linux/time.h \
  include/linux/bitops.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/bitops.h \
  include/asm-generic/bitops/fls64.h \
  include/asm-generic/bitops/hweight.h \
  include/linux/string.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/string.h \
    $(wildcard include/config/arm/optimzed/string/functions.h) \
  include/linux/kernel.h \
  include/linux/bug.h \
  include/asm-generic/bug.h \
  include/linux/barebox-wrapper.h \
  include/malloc.h \
  include/xfuncs.h \
  include/wchar.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/common.h \
  include/printk.h \
    $(wildcard include/config/compile/loglevel.h) \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/barebox.h \
    $(wildcard include/config/arm/unwind.h) \
    $(wildcard include/config/cpu/v8.h) \
    $(wildcard include/config/arm/exceptions.h) \
  include/fs.h \
  include/unistd.h \
  include/fcntl.h \
  include/sys/ioctl.h \
  include/dirent.h \
  include/sys/mount.h \
  include/sys/stat.h \
  include/linux/stat.h \
  include/filetype.h \
    $(wildcard include/config/arm.h) \
  include/linux/fs.h \
    $(wildcard include/config/quota.h) \
    $(wildcard include/config/dnotify.h) \
    $(wildcard include/config/inotify.h) \
    $(wildcard include/config/security.h) \
    $(wildcard include/config/epoll.h) \
    $(wildcard include/config/debug/writecount.h) \
    $(wildcard include/config/fs/dax.h) \
  include/linux/mount.h \
  include/linux/dcache.h \
    $(wildcard include/config/profiling.h) \
  include/linux/spinlock.h \
  include/linux/path.h \
  include/linux/mutex.h \
  include/linux/wait.h \
  include/linux/rwsem.h \
  include/fnmatch.h \
  include/qsort.h \
  include/glob.h \

lib/glob.o: $(deps_lib/glob.o)

$(deps_lib/glob.o):
