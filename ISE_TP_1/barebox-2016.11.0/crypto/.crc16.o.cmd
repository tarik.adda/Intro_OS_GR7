cmd_crypto/crc16.o := arm-none-eabi-gcc -Wp,-MD,crypto/.crc16.o.d  -nostdinc -isystem /usr/lib/gcc/arm-none-eabi/4.9.3/include -D__KERNEL__ -D__BAREBOX__ -Iinclude -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/dts/include  -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include -include /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/include/linux/kconfig.h -fno-builtin -ffreestanding -D__ARM__ -fno-strict-aliasing -marm -mlittle-endian -mabi=aapcs-linux -mno-thumb-interwork -D__LINUX_ARM_ARCH__=7 -march=armv7-a  -msoft-float  -Iarch/arm/mach-vexpress/include -DTEXT_BASE=0x63f00000 -fdata-sections -ffunction-sections -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -Werror-implicit-function-declaration -fno-strict-aliasing -fno-common -Os -pipe -mno-unaligned-access  -fno-stack-protector -Wno-unused-but-set-variable -Wno-trampolines  -fno-delete-null-pointer-checks -Wdeclaration-after-statement -Wno-pointer-sign    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(crc16)"  -D"KBUILD_MODNAME=KBUILD_STR(crc16)" -c -o crypto/crc16.o crypto/crc16.c

source_crypto/crc16.o := crypto/crc16.c

deps_crypto/crc16.o := \
  include/crc.h \
  include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
  include/linux/posix_types.h \
  include/linux/stddef.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/posix_types.h \
  include/asm-generic/posix_types.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/bitsperlong.h \
  include/asm-generic/bitsperlong.h \
    $(wildcard include/config/64bit.h) \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/types.h \

crypto/crc16.o: $(deps_crypto/crc16.o)

$(deps_crypto/crc16.o):
