cmd_commands/timeout.o := arm-none-eabi-gcc -Wp,-MD,commands/.timeout.o.d  -nostdinc -isystem /usr/lib/gcc/arm-none-eabi/4.9.3/include -D__KERNEL__ -D__BAREBOX__ -Iinclude -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/dts/include  -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include -include /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/include/linux/kconfig.h -fno-builtin -ffreestanding -D__ARM__ -fno-strict-aliasing -marm -mlittle-endian -mabi=aapcs-linux -mno-thumb-interwork -D__LINUX_ARM_ARCH__=7 -march=armv7-a  -msoft-float  -Iarch/arm/mach-vexpress/include -DTEXT_BASE=0x63f00000 -fdata-sections -ffunction-sections -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -Werror-implicit-function-declaration -fno-strict-aliasing -fno-common -Os -pipe -mno-unaligned-access  -fno-stack-protector -Wno-unused-but-set-variable -Wno-trampolines  -fno-delete-null-pointer-checks -Wdeclaration-after-statement -Wno-pointer-sign    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(timeout)"  -D"KBUILD_MODNAME=KBUILD_STR(timeout)" -c -o commands/timeout.o commands/timeout.c

source_commands/timeout.o := commands/timeout.c

deps_commands/timeout.o := \
  include/command.h \
    $(wildcard include/config/longhelp.h) \
    $(wildcard include/config/auto/complete.h) \
  include/linux/list.h \
    $(wildcard include/config/debug/list.h) \
  include/linux/stddef.h \
  include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
  include/linux/posix_types.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/posix_types.h \
  include/asm-generic/posix_types.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/bitsperlong.h \
  include/asm-generic/bitsperlong.h \
    $(wildcard include/config/64bit.h) \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/types.h \
  include/linux/stringify.h \
  include/errno.h \
  include/asm-generic/errno.h \
  include/linux/err.h \
  include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/kasan.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
    $(wildcard include/config/kprobes.h) \
  include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
    $(wildcard include/config/gcov/kernel.h) \
    $(wildcard include/config/arch/use/builtin/bswap.h) \
  include/getopt.h \
  include/environment.h \
    $(wildcard include/config/environment/variables.h) \
  include/console_countdown.h \
  include/linux/kernel.h \
  include/linux/bug.h \
  include/asm-generic/bug.h \
  include/linux/barebox-wrapper.h \
  include/malloc.h \
  include/types.h \
  include/xfuncs.h \
  /usr/lib/gcc/arm-none-eabi/4.9.3/include/stdarg.h \
  include/wchar.h \

commands/timeout.o: $(deps_commands/timeout.o)

$(deps_commands/timeout.o):
