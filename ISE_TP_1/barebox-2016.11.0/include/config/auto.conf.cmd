deps_config := \
	crypto/Kconfig \
	lib/bootstrap/Kconfig \
	lib/logo/Kconfig \
	lib/fonts/Kconfig \
	lib/gui/Kconfig \
	lib/lzo/Kconfig \
	lib/Kconfig \
	fs/squashfs/Kconfig \
	fs/pstore/Kconfig \
	fs/ubifs/Kconfig \
	fs/fat/Kconfig \
	fs/ext4/Kconfig \
	fs/Kconfig \
	drivers/crypto/caam/Kconfig \
	drivers/crypto/Kconfig \
	drivers/phy/Kconfig \
	drivers/firmware/Kconfig \
	drivers/rtc/Kconfig \
	drivers/pci/Kconfig \
	drivers/reset/Kconfig \
	drivers/regulator/Kconfig \
	drivers/bus/Kconfig \
	drivers/pinctrl/mvebu/Kconfig \
	drivers/pinctrl/Kconfig \
	drivers/w1/slaves/Kconfig \
	drivers/w1/masters/Kconfig \
	drivers/w1/Kconfig \
	drivers/gpio/Kconfig \
	drivers/dma/Kconfig \
	drivers/pwm/Kconfig \
	drivers/watchdog/Kconfig \
	drivers/input/Kconfig \
	drivers/eeprom/Kconfig \
	drivers/led/Kconfig \
	drivers/misc/Kconfig \
	drivers/mfd/Kconfig \
	drivers/clocksource/Kconfig \
	drivers/clk/Kconfig \
	drivers/mci/Kconfig \
	drivers/video/imx-ipu-v3/Kconfig \
	drivers/video/Kconfig \
	drivers/usb/musb/Kconfig \
	drivers/usb/gadget/Kconfig \
	drivers/usb/storage/Kconfig \
	drivers/usb/otg/Kconfig \
	drivers/usb/host/Kconfig \
	drivers/usb/imx/Kconfig \
	drivers/usb/Kconfig \
	drivers/ata/Kconfig \
	drivers/mtd/ubi/Kconfig \
	drivers/mtd/spi-nor/Kconfig \
	drivers/mtd/nand/Kconfig \
	drivers/mtd/nor/Kconfig \
	drivers/mtd/devices/Kconfig \
	drivers/mtd/Kconfig \
	drivers/i2c/muxes/Kconfig \
	drivers/i2c/busses/Kconfig \
	drivers/i2c/algos/Kconfig \
	drivers/i2c/Kconfig \
	drivers/spi/Kconfig \
	drivers/net/usb/Kconfig \
	drivers/net/phy/Kconfig \
	drivers/net/Kconfig \
	drivers/serial/Kconfig \
	drivers/amba/Kconfig \
	drivers/aiodev/Kconfig \
	drivers/of/Kconfig \
	drivers/Kconfig \
	net/Kconfig \
	commands/Kconfig \
	common/partitions/Kconfig \
	pbl/Kconfig \
	common/Kconfig \
	arch/arm/mach-qemu/Kconfig \
	arch/arm/mach-zynq/Kconfig \
	arch/arm/mach-uemd/Kconfig \
	arch/arm/mach-tegra/Kconfig \
	arch/arm/mach-vexpress/Kconfig \
	arch/arm/boards/versatile/Kconfig \
	arch/arm/mach-versatile/Kconfig \
	arch/arm/mach-socfpga/Kconfig \
	arch/arm/boards/friendlyarm-tiny6410/Kconfig \
	arch/arm/boards/friendlyarm-mini2440/Kconfig \
	arch/arm/mach-samsung/Kconfig \
	arch/arm/mach-rockchip/Kconfig \
	arch/arm/mach-pxa/Kconfig \
	arch/arm/mach-omap/Kconfig \
	arch/arm/mach-nomadik/Kconfig \
	arch/arm/mach-netx/Kconfig \
	arch/arm/mach-mvebu/Kconfig \
	arch/arm/mach-mxs/Kconfig \
	arch/arm/mach-imx/Kconfig \
	arch/arm/mach-highbank/Kconfig \
	arch/arm/mach-ep93xx/Kconfig \
	arch/arm/mach-digic/Kconfig \
	arch/arm/mach-davinci/Kconfig \
	arch/arm/mach-clps711x/Kconfig \
	arch/arm/mach-bcm283x/Kconfig \
	arch/arm/mach-at91/Kconfig \
	arch/arm/cpu/Kconfig \
	arch/arm/Kconfig \
	Kconfig

include/config/auto.conf: \
	$(deps_config)

ifneq "$(KERNELVERSION)" "2016.11.0"
include/config/auto.conf: FORCE
endif
ifneq "$(ARCH)" "arm"
include/config/auto.conf: FORCE
endif
ifneq "$(SRCARCH)" "arm"
include/config/auto.conf: FORCE
endif

$(deps_config): ;
