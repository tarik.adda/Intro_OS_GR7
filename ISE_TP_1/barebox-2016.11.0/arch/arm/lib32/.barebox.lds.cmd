cmd_arch/arm/lib32/barebox.lds := arm-none-eabi-gcc -E -Wp,-MD,arch/arm/lib32/.barebox.lds.d  -nostdinc -isystem /usr/lib/gcc/arm-none-eabi/4.9.3/include -D__KERNEL__ -D__BAREBOX__ -Iinclude -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/dts/include  -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include -include /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/include/linux/kconfig.h -fno-builtin -ffreestanding -D__ARM__ -fno-strict-aliasing -marm -mlittle-endian -mabi=aapcs-linux -mno-thumb-interwork -D__LINUX_ARM_ARCH__=7 -march=armv7-a  -msoft-float  -Iarch/arm/mach-vexpress/include -DTEXT_BASE=0x63f00000 -fdata-sections -ffunction-sections  -C -Uarm -P -D__ASSEMBLY__ -o arch/arm/lib32/barebox.lds arch/arm/lib32/barebox.lds.S

source_arch/arm/lib32/barebox.lds := arch/arm/lib32/barebox.lds.S

deps_arch/arm/lib32/barebox.lds := \
    $(wildcard include/config/relocatable.h) \
    $(wildcard include/config/pbl/image.h) \
    $(wildcard include/config/arm/unwind.h) \
  include/asm-generic/barebox.lds.h \
    $(wildcard include/config/x86.h) \
    $(wildcard include/config/arch/ep93xx.h) \
    $(wildcard include/config/arch/zynq.h) \
    $(wildcard include/config/arch/barebox/max/bare/init/size.h) \
    $(wildcard include/config/barebox/max/bare/init/size.h) \
    $(wildcard include/config/arch/barebox/max/pbl/size.h) \
    $(wildcard include/config/barebox/max/pbl/size.h) \
  include/linux/stringify.h \

arch/arm/lib32/barebox.lds: $(deps_arch/arm/lib32/barebox.lds)

$(deps_arch/arm/lib32/barebox.lds):
