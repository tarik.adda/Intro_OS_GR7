cmd_arch/arm/cpu/common.o := arm-none-eabi-gcc -Wp,-MD,arch/arm/cpu/.common.o.d  -nostdinc -isystem /usr/lib/gcc/arm-none-eabi/4.9.3/include -D__KERNEL__ -D__BAREBOX__ -Iinclude -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/dts/include  -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include -I/home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include -include /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/include/linux/kconfig.h -fno-builtin -ffreestanding -D__ARM__ -fno-strict-aliasing -marm -mlittle-endian -mabi=aapcs-linux -mno-thumb-interwork -D__LINUX_ARM_ARCH__=7 -march=armv7-a  -msoft-float  -Iarch/arm/mach-vexpress/include -DTEXT_BASE=0x63f00000 -fdata-sections -ffunction-sections -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -Werror-implicit-function-declaration -fno-strict-aliasing -fno-common -Os -pipe -mno-unaligned-access  -fno-stack-protector -Wno-unused-but-set-variable -Wno-trampolines  -fno-delete-null-pointer-checks -Wdeclaration-after-statement -Wno-pointer-sign    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(common)"  -D"KBUILD_MODNAME=KBUILD_STR(common)" -c -o arch/arm/cpu/common.o arch/arm/cpu/common.c

source_arch/arm/cpu/common.o := arch/arm/cpu/common.c

deps_arch/arm/cpu/common.o := \
  include/common.h \
    $(wildcard include/config/banner.h) \
    $(wildcard include/config/mips.h) \
  include/stdio.h \
    $(wildcard include/config/console/none.h) \
    $(wildcard include/config/pbl/console.h) \
  /usr/lib/gcc/arm-none-eabi/4.9.3/include/stdarg.h \
  include/console.h \
    $(wildcard include/config/cbsize.h) \
    $(wildcard include/config/prompt.h) \
  include/param.h \
    $(wildcard include/config/parameter.h) \
  include/linux/err.h \
  include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/kasan.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
    $(wildcard include/config/kprobes.h) \
  include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
    $(wildcard include/config/gcov/kernel.h) \
    $(wildcard include/config/arch/use/builtin/bswap.h) \
  include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
  include/linux/posix_types.h \
  include/linux/stddef.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/posix_types.h \
  include/asm-generic/posix_types.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/bitsperlong.h \
  include/asm-generic/bitsperlong.h \
    $(wildcard include/config/64bit.h) \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/types.h \
  include/asm-generic/errno.h \
  include/linux/list.h \
    $(wildcard include/config/debug/list.h) \
  include/driver.h \
    $(wildcard include/config/driver/net/dm9k.h) \
    $(wildcard include/config/usb/ehci.h) \
    $(wildcard include/config/driver/net/ks8851/mll.h) \
    $(wildcard include/config/ofdevice.h) \
  include/linux/ioport.h \
  include/of.h \
    $(wildcard include/config/oftree.h) \
  include/fdt.h \
  include/errno.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/byteorder.h \
  include/linux/byteorder/little_endian.h \
  include/linux/swab.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/swab.h \
  include/linux/byteorder/generic.h \
  include/module.h \
    $(wildcard include/config/modules.h) \
  include/elf.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/elf.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/ptrace.h \
    $(wildcard include/config/arm/thumb.h) \
  include/config.h \
  include/clock.h \
  include/types.h \
  include/linux/time.h \
  include/linux/bitops.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/bitops.h \
  include/asm-generic/bitops/fls64.h \
  include/asm-generic/bitops/hweight.h \
  include/linux/string.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/string.h \
    $(wildcard include/config/arm/optimzed/string/functions.h) \
  include/linux/kernel.h \
  include/linux/bug.h \
  include/asm-generic/bug.h \
  include/linux/barebox-wrapper.h \
  include/malloc.h \
  include/xfuncs.h \
  include/wchar.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/common.h \
  include/printk.h \
    $(wildcard include/config/compile/loglevel.h) \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/barebox.h \
    $(wildcard include/config/arm/unwind.h) \
    $(wildcard include/config/cpu/v8.h) \
    $(wildcard include/config/arm/exceptions.h) \
  include/init.h \
  include/linux/sizes.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/system_info.h \
    $(wildcard include/config/cpu/32v4t.h) \
    $(wildcard include/config/cpu/32v5.h) \
    $(wildcard include/config/cpu/32v6.h) \
    $(wildcard include/config/cpu/32v7.h) \
    $(wildcard include/config/cpu/64v8.h) \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/cputype.h \
    $(wildcard include/config/cpu/xsc3.h) \
    $(wildcard include/config/cpu/xscale.h) \
  include/linux/stringify.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/barebox-arm.h \
    $(wildcard include/config/relocatable.h) \
    $(wildcard include/config/fs/pstore/ramoops.h) \
    $(wildcard include/config/fs/pstore/ramoops/size.h) \
  include/asm-generic/memory_layout.h \
    $(wildcard include/config/memory/layout/default.h) \
    $(wildcard include/config/malloc/size.h) \
    $(wildcard include/config/stack/size.h) \
    $(wildcard include/config/memory/layout/fixed.h) \
    $(wildcard include/config/stack/base.h) \
    $(wildcard include/config/malloc/base.h) \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/barebox-arm-head.h \
    $(wildcard include/config/have/mach/arm/head.h) \
    $(wildcard include/config/thumb2/barebox.h) \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/system.h \
    $(wildcard include/config/cpu/fa526.h) \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/sections.h \
  include/asm-generic/sections.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/pgtable.h \
  /home/etudiant/Intro_OS_GR7/ISE_TP_1/barebox-2016.11.0/arch/arm/include/asm/cache.h \

arch/arm/cpu/common.o: $(deps_arch/arm/cpu/common.o)

$(deps_arch/arm/cpu/common.o):
