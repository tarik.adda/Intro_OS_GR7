Pour la création de u-boot selon l'arm cortex a9 on exécute qemu-system-arm -M ? | grep cortex a9. 
On trouve l'image vexpress-a9 qui correspond.On navigue dans les fichiers include/configs. On trouve le header vexpress_ca9x4 qui correspond à l'image qu'on veut simuler.

On configure alors u-boot à l'aide du fichier conf repéré. Le makefile doit être préparé à l'aide des variable CROSS_COMPILE et ARCH soit :
export CROSS_COMPILE=arm-linux-gnueabi-
export ARCH=arm
make vexpress_ca9x4_config

On peut ensuite lancer make all pour avoir les fichier u-boot.

On peut maintenant simuler l'archi avec le cortex a9, soit: qemu-system-arm -M vexpress-a9 -m 256 -kernel u-boot (l'option -m lui donne un peu de RAM c'est optionnel)

On obtient plusieurs fichiers dont u-boot et u-boot.bin. 
En utilisant file on voit que le bin, est un binaire et le u-boot est un fichier elf. Le u-boot contient les définitions de fonctions, les informations sur le code ainsi que des fichiers de débug, donc le u-boot sert pour le débogage. 
Ainsi en désactivant ces options (dans le .config qu'on peut manipuler) on peut avoir un fichier u-boot moins conséquent.

Pour customiser le message de prompt il a fallut toucher au fichier de config .config et changer la ligne CONFIG_SYS_PROMPT="blab lab la"

Pour le débug on précise à qemu qu'on va débug lors de l'émulation à l'aide de la commande: qemu-system-arm -M vexpress-a9 -m 256 -kernel u-boot -s -S. On utilise le fichier u-boot pour débug et les options -s (pour le débug) et -S (pour attendre gdb).

On ouvre un autre terminal et on utilise gdb-multiarch u-boot . On entre la cible: target remote localhost:1234 (ici localhost est l'ip de cette machine)
On a accès maintenant au code avec gdb, on peut afficher le code source avec list, mettre des breakpoint avec b, reprendre l'éxécution avec c et faire de l'éxécution pas à pas avec step. Cependant u-boot se charge dans une autre mémoire à la fonction board-init-f, on y met donc un breakpoint pour pouvoir recharger les symboles à l'adresse calculé.


Pour rajouter une commande u-boot on va dans le dossier common pour ajouter un fichier source de format cmd_countdown.c on modifie ensuite le makefile de ce dossier pour lui signifier qu'on va créer le fichier objet à partir de ce source, puis on fait un make all pour recréer les fichier u-boot.
