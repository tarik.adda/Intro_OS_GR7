Manipulations noyau Linux (durée approximative : 1h)

1. Télécharger les sources mainline (depuis kernel.org) de la version 4.3 du noyau Linux et la transformer en
4.4.37 grâce aux patchs. En exploitant l’outil meld, constater que le résultat est identique à l’archive de sources
en version 4.4.37. (0.5 point)

>Dans un 1er temps on décompresse l'archive de linux-4.3.tar.xz avec la commande suivante : 

		tar -xf linux-4.3.tar.xz

>On applique ensuite les patchs de la façon suivante : 

		xzcat ../patch-4.4.xz | patch -p1
		xzcat ../patch-4.4.37.xz | patch -p1

>Et en comparant avec l'outil meld (Commande meld dans le shell) on constate qu'il n'y a aucune différence.


2. Utiliser cscope pour trouver où est implémentée la fonction omap_gpio_init_irq. Par quel autre(s) moyen(s)
aurait-on pu trouver cette information ?

>Il faut dans un premier temps construire la bdd  avec la commande suivante : 
	
		cscope -R

>Ensuite chercher la fonction dans cscope

		find this C symbol: omap_gpio_init_irq

>Les résultats associés 

		0 gpio-omap.c omap_gpio_init_irq 477 static void omap_gpio_init_irq(struct gpio_bank *bank, unsigned offset)
		1 gpio-omap.c omap_gpio_irq_type 506 omap_gpio_init_irq(bank, offset);

>Une autre façon est le grep find.

3. Trouver l’emplacement du logo tux dans les sources du noyau. Pourquoi cet emplacement est-il utile ?

		cscope -R

>localisé dans documentation

>Utile Car il y a de la documentation..

4. Quelle version de noyau est actuellement utilisée sous Ubuntu ? Utiliser les fichiers de configuration et de
boot associés à cette version pour compiler depuis les sources mainline un noyau équivalent. Le démarrer sous
Qemu (attention à la cible utilisée en émulation Qemu !) (0.5 point)
Le démarrage est-il complet ? Est-ce normal ?


		uname -r

>4.4.0-62-generic

		tar xf linux-4.4.tar.xz

>Copier config-4.4.0-62-generic du boot de la machine dans le dossier décompresser à la racine

		make config-4.2.0-42-generic

>renomer ce fichier de config en .config

>lancer la compile avec : (multi coeur)

		make -j4

>Démarrage sous Qemu 

		qemu-system-x86_64  -kernel arch/x86/boot/bzImage -initrd /boot/initrd.img-4.4.0-62-generic -serial stdio  -append 'console=ttyS0'




Cross-compilation noyau Linux (durée approximative : 3h)

1. Télécharger les sources mainline d’un noyau version 4.8.12 et le cross-compiler pour une configuration Ver-
satile Express Cortex-A9. Le démarrer sous Qemu en utilisant la commande qemu-system-arm avec les bons
arguments (voir TP précédent). Attention, ces versions récentes de noyaux utilisent tous la déclaration de ma-
tériel sous forme de Device Tree ! (1 point)
Quelle option de Qemu permet d’émuler plusieurs processeurs ?
Le démarrage du noyau est-il complet ? Est-ce normal ?

>Definition des variables
		export ARCH=arm
		export CROSS_COMPILE=arm-linux-gnueabi
>Déclaration du .config
		make vexpress_defconfig
>Cross crompilation pour une configuration versatille 
		make ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- -j4

		qemu-system-arm -M vexpress-a9 -kernel arch/arm/boot/zImage -m 1024 -serial stdio -append 'console=ttyAMA0' -dtb arch/arm/boot/dts/vexpress-v2p-ca9.dtb

2. En utilisant le code source suivant, constituer un rootfs minimal à utiliser sous Qemu conjointement au noyau
précédemment cross-compilé. Attention à utiliser la bonne cross-toolchain ! (1 point)

#include <stdio.h>
void main() {
printf("Hello World!\n");
while(1);
}

Pour générer une image vide du rootfs, dans laquelle on pourra intégrer un exécutable basique basé sur le code
source précédent, on utilisera les commandes suivantes :


>>> dd if=/dev/zero of=./vexpress.img bs=4M count=1024
>>> sudo losetup -f ./vexpress.img
>>> sudo mkfs.ext4 /dev/loop0

Expliquer le rôle de chaque commande.

>	Copie et Colle le disque de /dev/zero vers /vexpress.img
		>>> dd if=/dev/zero of=./vexpress.img bs=4M count=1024
>	Control et régule les systèmes de boucles  de vexpress.img
		>>> sudo losetup -f ./vexpress.img
>	Formattage
		>>> sudo mkfs.ext4 /dev/loop0



3. Modifier la configuration du noyau pour que le logo tux au démarrage s’affiche en noir et blanc et qu’un
timestamp soit affiché pour chaque ligne du log de démarrage. Le recompiler et le démarrer sous Qemu pour
constater la bonne application des modifications. (1 point)


>Changer la couleur du logo en noir et blanc 
		make menuconfig
		
>Il faut aller dans Devices Drivers / Graphics support / bootup logo et choisir Standard black and white linux logo

>Ajouter un timestamp, il faut aller dans kernel hackking / printk and dmesg options / activer show timing information on printks


4. Utiliser Busybox version 1.25.1 pour générer un rootfs minimal à utiliser sous Qemu conjointement au noyau
précédemment compilé. On utilise le même principe que précédemment, mais au lieu d’un simple exécutable,
on utilisera Busybox (pour simplifier le déploiement Busybox doit être compilé en statique !). (1 point)
Le démarrage est-il satisfaisant ? Pourquoi obtient-on une erreur ? Comment peut-on la résoudre ?



5. Télécharger les sources mainline d’un noyau version 3.2 et le cross-compiler pour une configuration Versatile
Express Cortex-A9. Le démarrer sous Qemu en utilisant la commande qemu-system-arm avec les bons argu-
ments (voir TP précédent). Attention, ces versions plus anciennes de noyaux n’utilisent pas la déclaration de
matériel sous forme de Device Tree ! (1 point)
Pourquoi la compilation échoue-t-elle ? Comment peut-on contourner le problème (2 solutions possibles) ?

